<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/shop', 'ShopController@index');
Route::get('/cart', 'ShopController@cart');
Route::get('/add_to_cart/{id}', 'ShopController@add_to_cart');
Route::get('/remove/{id}', 'ShopController@remove_from_cart');
Route::get('/remove_all', 'ShopController@delete_all');
Route::post('/submit', 'ShopController@submit');
Route::get('/purchase_history', 'OrderController@index');
Route::get('/users', 'UserController@show');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::get('/change_password', 'UserController@edit_password');
Route::match(['put', 'patch'], '/users/{id}', 'UserController@update');
Route::match(['put', 'patch'], '/users/{id}/cp', 'UserController@update_password');
Route::resource('books', 'BookController');

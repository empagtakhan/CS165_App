<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared("CREATE OR REPLACE FUNCTION availability_function() RETURNS trigger AS $$
       BEGIN
        IF NEW.available_items<0 THEN
         RAISE EXCEPTION 'negative number not allowed';
        ELSIF NEW.available_items > 0 THEN
         NEW.status = 'available';
        ELSE
         NEW.status = 'unavailable';
        END IF;
        RETURN NEW;
      END;
      $$ LANGUAGE plpgsql;");

      DB::unprepared("CREATE TRIGGER availability_trigger
       BEFORE INSERT OR UPDATE ON books
      FOR EACH ROW EXECUTE PROCEDURE availability_function();");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

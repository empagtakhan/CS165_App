<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('orders')->insert([
        'order_number' => 1,
        'customer_id' => 6
      ]);

      DB::table('orders')->insert([
        'order_number' => 2,
        'customer_id' => 9
      ]);

      DB::table('orders')->insert([
        'order_number' => 3,
        'customer_id' => 11
      ]);

      DB::table('orders')->insert([
        'order_number' => 4,
        'customer_id' => 14
      ]);

      DB::table('orders')->insert([
        'order_number' => 5,
        'customer_id' => 7
      ]);

      DB::table('orders')->insert([
        'order_number' => 6,
        'customer_id' => 6
      ]);

      DB::table('orders')->insert([
        'order_number' => 7,
        'customer_id' => 10
      ]);

      DB::table('orders')->insert([
        'order_number' => 8,
        'customer_id' => 12
      ]);

      DB::table('orders')->insert([
        'order_number' => 9,
        'customer_id' => 11
      ]);

      DB::table('orders')->insert([
        'order_number' => 10,
        'customer_id' => 8
      ]);

      DB::table('orders')->insert([
        'order_number' => 11,
        'customer_id' => 14
      ]);

      DB::table('orders')->insert([
        'order_number' => 12,
        'customer_id' => 15
      ]);

      DB::table('orders')->insert([
        'order_number' => 13,
        'customer_id' => 9
      ]);

      DB::table('orders')->insert([
        'order_number' => 14,
        'customer_id' => 7
      ]);

      DB::table('orders')->insert([
        'order_number' => 15,
        'customer_id' => 8
      ]);

      DB::table('orders')->insert([
        'order_number' => 16,
        'customer_id' => 11
      ]);

      DB::table('orders')->insert([
        'order_number' => 17,
        'customer_id' => 12
      ]);

      DB::table('orders')->insert([
        'order_number' => 18,
        'customer_id' => 6
      ]);

      DB::table('orders')->insert([
        'order_number' => 19,
        'customer_id' => 10
      ]);

      DB::table('orders')->insert([
        'order_number' => 20,
        'customer_id' => 13
      ]);
    }
}

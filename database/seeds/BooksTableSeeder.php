<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('books')->insert([
        'book_name' => 'Nineteen Eighty-Four',
        'description' =>  'A dystopian novel by English author George Orwell published in 1949. The novel is set in Airstrip One (formerly known as Great Britain), a province of the superstate Oceania in a world of perpetual war, omnipresent government surveillance, and public manipulation, dictated by a political system euphemistically named English Socialism (or Ingsoc in the government\'s invented language, Newspeak) under the control of a privileged elite of the Inner Party, that persecutes individualism and independent thinking as "thoughtcrime."',
        'isVisible' => 1,
        'available_items' => 5
      ]);

      DB::table('books')->insert([
        'book_name' => 'The Sound and the Fury',
        'description' =>  'A novel written by the American author William Faulkner. It employs a number of narrative styles, including the technique known as stream of consciousness, pioneered by 20th-century European novelists such as James Joyce and Virginia Woolf.',
        'isVisible' => 1,
        'available_items' => 3
      ]);

      DB::table('books')->insert([
        'book_name' => 'To Kill a Mockingbird',
        'description' =>  'A novel by Harper Lee published in 1960. It was immediately successful, winning the Pulitzer Prize, and has become a classic of modern American literature. The plot and characters are loosely based on the author\'s observations of her family and neighbors, as well as on an event that occurred near her hometown in 1936, when she was 10 years old.',
        'isVisible' => 1,
        'available_items' => 0
      ]);

      DB::table('books')->insert([
        'book_name' => "Harry Potter and the Philosopher's Stone",
        'description' =>  'The first novel in the Harry Potter series and J. K. Rowling\'s debut novel, first published in 1997 by Bloomsbury. It was published in the United States as Harry Potter and the Sorcerer\'s Stone by Scholastic Corporation in 1998. The plot follows Harry Potter, a young wizard who discovers his magical heritage as he makes close friends and a few enemies in his first year at the Hogwarts School of Witchcraft and Wizardry. With the help of his friends, Harry faces an attempted comeback by the dark wizard Lord Voldemort, who killed Harry\'s parents, but failed to kill Harry when he was just a year old.',
        'isVisible' => 1,
        'available_items' => 20
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Chamber of Secrets',
        'description' =>  'The second novel in the Harry Potter series, written by J. K. Rowling. The plot follows Harry\'s second year at Hogwarts School of Witchcraft and Wizardry, during which a series of messages on the walls of the school\'s corridors warn that the "Chamber of Secrets" has been opened and that the "heir of Slytherin" would kill all pupils who do not come from all-magical families. These threats are found after attacks which leave residents of the school "petrified" (frozen like stone). Throughout the year, Harry and his friends Ronald Weasley and Hermione Granger investigate the attacks.',
        'isVisible' => 1,
        'available_items' => 14
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Prisoner of Azkaban',
        'description' =>  'The third novel in the Harry Potter series, written by J. K. Rowling. The book follows Harry Potter, a young wizard, in his third year at Hogwarts School of Witchcraft and Wizardry. Along with friends Ron Weasley and Hermione Granger, Harry investigates Sirius Black, an escaped prisoner from Azkaban who they believe is one of Lord Voldemort\'s old allies.',
        'isVisible' => 1,
        'available_items' => 0
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Goblet of Fire',
        'description' =>  'The fourth novel in the Harry Potter series, written by British author J. K. Rowling. It follows Harry Potter, a wizard in his fourth year at Hogwarts School of Witchcraft and Wizardry and the mystery surrounding the entry of Harry\'s name into the Triwizard Tournament, in which he is forced to compete.',
        'isVisible' => 1,
        'available_items' => 9
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Order of the Phoenix',
        'description' =>  'The fifth novel in the Harry Potter series, written by J. K. Rowling. It follows Harry Potter\'s struggles through his fifth year at Hogwarts School of Witchcraft and Wizardry, including the surreptitious return of the antagonist Lord Voldemort, O.W.L. exams, and an obstructive Ministry of Magic. The novel was published on 21 June 2003 by Bloomsbury in the United Kingdom, Scholastic in the United States, and Raincoast in Canada. Five million copies were sold in the first 24 hours of publication.[1] It is the longest book of the series.',
        'isVisible' => 1,
        'available_items' => 15
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Half-Blood Prince',
        'description' =>  'The sixth and penultimate novel in the Harry Potter series, written by British author J. K. Rowling. Set during protagonist Harry Potter\'s sixth year at Hogwarts, the novel explores the past of Harry\'s nemesis, Lord Voldemort, and Harry\'s preparations for the final battle against Voldemort alongside his headmaster and mentor Albus Dumbledore.',
        'isVisible' => 1,
        'available_items' => 2
      ]);

      DB::table('books')->insert([
        'book_name' => 'Harry Potter and the Deathly Hallows',
        'description' =>  'The seventh and final novel of the Harry Potter series, written by British author J. K. Rowling. The book was released on 21 July 2007 by Bloomsbury Publishing in the United Kingdom, in the United States by Scholastic, and in Canada by Raincoast Books, ending the series that began in 1997 with the publication of Harry Potter and the Philosopher\'s Stone. The novel chronicles the events directly following Harry Potter and the Half-Blood Prince (2005), and the final confrontation between the wizards Harry Potter and Lord Voldemort, as well as revealing the previously concealed back story of several main characters. The title of the book refers to three mythical objects featured in the story, collectively known as the "Deathly Hallows"—an unbeatable wand, a stone to bring the dead to life, and a cloak of invisibility.',
        'isVisible' => 1,
        'available_items' => 12
      ]);

      DB::table('books')->insert([
        'book_name' => 'Algorithms + Data Structures = Programs',
        'description' =>  'a 1976 book written by Niklaus Wirth covering some of the fundamental topics of computer programming, particularly that algorithms and data structures are inherently related. For example, if one has a sorted list one will use a search algorithm optimal for sorted lists.',
        'isVisible' => 1,
        'available_items' => 5
      ]);

      DB::table('books')->insert([
        'book_name' => 'The Great Gatsby',
        'description' =>  'A 1925 novel written by American author F. Scott Fitzgerald that follows a cast of characters living in the fictional town of West Egg on prosperous Long Island in the summer of 1922. The story primarily concerns the young and mysterious millionaire Jay Gatsby and his quixotic passion and obsession for the beautiful former debutante Daisy Buchanan. Considered to be Fitzgerald\'s magnum opus, The Great Gatsby explores themes of decadence, idealism, resistance to change, social upheaval, and excess, creating a portrait of the Jazz Age or the Roaring Twenties that has been described as a cautionary tale regarding the American Dream.',
        'isVisible' => 1,
        'available_items' => 7
      ]);

      DB::table('books')->insert([
        'book_name' => 'Tender Is the Night ',
        'description' =>  'a novel by American writer F. Scott Fitzgerald. It was his fourth and final completed novel, and was first published in Scribner\'s Magazine between January and April 1934 in four issues. The title is taken from the poem "Ode to a Nightingale" by John Keats.',
        'isVisible' => 1,
        'available_items' => 6
      ]);

      DB::table('books')->insert([
        'book_name' => 'Miss Peregrine\'s Home for Peculiar Children',
        'description' =>  'The debut novel by American author Ransom Riggs. It is a story of a boy who, following a horrific family tragedy, follows clues that take him to an abandoned children\'s home on a Welsh island. The story is told through a combination of narrative and vernacular photographs from the personal archives of collectors listed by the author.',
        'isVisible' => 1,
        'available_items' => 17
      ]);

      DB::table('books')->insert([
        'book_name' => 'Hollow City',
        'description' =>  'A sequel to 2011 novel Miss Peregrine\'s Home for Peculiar Children written by Ransom Riggs. It was released on January 14, 2014 by Quirk Books.[1] The novel is set right after the first, and sees Jacob and his friends fleeing from Miss Peregrine\'s to the "peculiar capital of the world", London.',
        'isVisible' => 1,
        'available_items' => 12
      ]);

      DB::table('books')->insert([
        'book_name' => 'Introduction to the Theory of Computation',
        'description' =>  'A standard textbook in theoretical computer science, written by Michael Sipser and first published by PWS Publishing in 1997.',
        'isVisible' => 1,
        'available_items' => 8
      ]);

      DB::table('books')->insert([
        'book_name' => 'Introduction to Automata Theory, Languages, and Computation',
        'description' =>  'An influential computer science textbook by John Hopcroft and Jeffrey Ullman on formal languages and the theory of computation.',
        'isVisible' => 1,
        'available_items' => 5
      ]);

      DB::table('books')->insert([
        'book_name' => 'Divergent',
        'description' =>  'the debut novel of American novelist Veronica Roth, published by HarperCollins Children\'s Books in 2011. The novel is the first of the Divergent trilogy, a series of young adult dystopian novels set in the Divergent Universe. The novel Divergent features a post-apocalyptic version of Chicago and follows Beatrice "Tris" Prior as she explores her identity within a society that defines its citizens by their social and personality-related affiliation with five factions, which removes the threat of anyone exercising independent will and re-threatening the population\'s safety. Underlying the action and dystopian focused main plot is a romantic subplot between Tris and one of her instructors in the Dauntless faction, nicknamed Four.',
        'isVisible' => 1,
        'available_items' => 8
      ]);

      DB::table('books')->insert([
        'book_name' => 'Insurgent',
        'description' =>  'a 2012 science fiction young adult novel by American novelist Veronica Roth and the second book in the Divergent trilogy. As the sequel to the 2011 bestseller Divergent, it continues the story of Tris Prior and the dystopian post-apocalyptic version of Chicago. Following the events of the previous novel, a war now looms as conflict between the factions and their ideologies grows. While trying to save the people that she loves, Tris faces questions of grief, forgiveness, identity, loyalty, politics, and love.',
        'isVisible' => 1,
        'available_items' => 3
      ]);

      DB::table('books')->insert([
        'book_name' => 'Allegiant',
        'description' =>  'a science fiction novel for young adults, written by the American author Veronica Roth and published by HarperCollins in October 2013. It completes the Divergent trilogy that Roth started with her debut novel Divergent in 2011. The book is written from the perspective of both Beatrice (Tris) and Tobias (Four). Following the revelations of the previous novel, they journey past the city\'s boundaries to discover what lies beyond.',
        'isVisible' => 1,
        'available_items' => 0
      ]);
    }

}

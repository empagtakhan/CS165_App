<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('users')->insert([
        'first_name' => 'Edison',
        'last_name' => 'Pagtakhan',
        'email' =>  'edisonpagtakhan@shop.com',
        'password' => bcrypt('123456789'),
        'role' => 'admin'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Sue',
        'last_name' => 'King',
        'email' =>  'sueking@shop.com',
        'password' => bcrypt('123456789'),
        'role' => 'admin'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Peter',
        'last_name' => 'Ferguson',
        'email' =>  'peterferguson@shop.com',
        'password' => bcrypt('123456789'),
        'role' => 'admin'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Valarie',
        'last_name' => 'Nelson',
        'email' =>  'valarienelson@shop.com',
        'password' => bcrypt('123456789'),
        'role' => 'admin'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Roland',
        'last_name' => 'Keitel',
        'email' =>  'rolandkeitel@shop.com',
        'password' => bcrypt('123456789'),
        'role' => 'admin'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Janine',
        'last_name' => 'Murphy',
        'email' =>  'janinemurphy@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Julie',
        'last_name' => 'Murphy',
        'email' =>  'juliemurphy@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Kwai',
        'last_name' => 'Yu',
        'email' =>  'kwaiyu@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Diego',
        'last_name' => 'Freyre',
        'email' =>  'diegofreyre@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Mary',
        'last_name' => 'Saveley',
        'email' =>  'marysaveley@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Eric',
        'last_name' => 'Natividad',
        'email' =>  'ericnatividad@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Jeff',
        'last_name' => 'Young',
        'email' =>  'jeffyoung@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Juri',
        'last_name' => 'Hirano',
        'email' =>  'jurihirano@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Wendy',
        'last_name' => 'Victorino',
        'email' =>  'wendyvictorino@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);

      DB::table('users')->insert([
        'first_name' => 'Isabel',
        'last_name' => 'De Castro',
        'email' =>  'isabeldecastro@shop.com',
        'password' => bcrypt('000000000'),
        'role' => 'buyer'
      ]);
    }
}

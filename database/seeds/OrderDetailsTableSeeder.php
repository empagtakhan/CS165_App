<?php

use Illuminate\Database\Seeder;

class OrderDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('orderdetails')->insert([
        'product_id' => 1,
        'order_number' => 1,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 2,
        'order_number' => 1,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 18,
        'order_number' => 2,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 19,
        'order_number' => 2,
        'quantity' => 1
      ]);
      DB::table('orderdetails')->insert([
        'product_id' => 20,
        'order_number' => 2,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 17,
        'order_number' => 3,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 5,
        'order_number' => 4,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 6,
        'order_number' => 4,
        'quantity' => 1
      ]);
      DB::table('orderdetails')->insert([
        'product_id' => 9,
        'order_number' => 5,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 16,
        'order_number' => 6,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 7,
        'order_number' => 7,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 6,
        'order_number' => 7,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 5,
        'order_number' => 8,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 4,
        'order_number' => 8,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 11,
        'order_number' => 9,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 12,
        'order_number' => 10,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 13,
        'order_number' => 11,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 14,
        'order_number' => 12,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 4,
        'order_number' => 13,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 5,
        'order_number' => 13,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 6,
        'order_number' => 13,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 7,
        'order_number' => 13,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 8,
        'order_number' => 14,
        'quantity' => 3
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 9,
        'order_number' => 14,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 10,
        'order_number' => 14,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 16,
        'order_number' => 15,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 1,
        'order_number' => 16,
        'quantity' => 2
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 2,
        'order_number' => 17,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 3,
        'order_number' => 17,
        'quantity' => 1
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 15,
        'order_number' => 18,
        'quantity' => 3
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 11,
        'order_number' => 19,
        'quantity' => 5
      ]);

      DB::table('orderdetails')->insert([
        'product_id' => 12,
        'order_number' => 20,
        'quantity' => 1
      ]);
    }
}

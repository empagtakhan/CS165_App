@extends('layouts.app')
@section('title', 'User Profile')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(Session::has('message'))
              <div class = "alert alert-success">{{ Session::get('message') }}</div>
            @endif
            <div class="panel panel-default">
              <div class="panel-heading">Dashboard <span style="padding-left: 550px;"><a href="/users/{{ $auth->id }}/edit"><u>Edit my profile</u></a></span></div>

                <div class="panel-body">
                    User Profile: <br><br>
                    Name: {{$auth->first_name}} {{ $auth->last_name}}<br>
                    @if($auth->role == 'admin')
                      Admin ID:
                    @else
                      User ID:
                    @endif
                    {{ $auth->id }}<br>
                    Email: {{$auth->email}}<br><br>

                    @if($auth->role == 'buyer')
                      Purchase History:
                      @php
                        $ordernum = 0;
                      @endphp
                      @foreach($purchases as $purchase)
                        @if($ordernum != $purchase->order_number)
                          <hr> Order Number: {{ $purchase->order_number}} <br><br>
                          @php
                            $ordernum = $purchase->order_number
                          @endphp
                        @endif
                        Product: {{ $purchase->book_name}}<br>
                        Quantity: {{$purchase->quantity}}<br>
                      @endforeach
                    @endif
                    <center> {{ $purchases->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

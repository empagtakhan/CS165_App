@extends('layouts.app')
@section('title', 'Purchase History')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Purchase History</div>
                <div class="panel-body">
                  @foreach($purchases as $purchase)
                  <div class="col-sm-4">
                    Customer Name: {{$purchase->first_name}} {{$purchase->last_name}} <br>
                    Order Number: {{$purchase->order_number}} <br>
                    <button type="submit" class="btn" data-toggle="modal" data-target="#orderModal{{$purchase->order_number}}">
                      <a>View Details</a>
                    </button><hr>
                  </div>
                  <div id="orderModal{{$purchase->order_number}}" class="modal fade" role = "dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Order Number {{$purchase->order_number}}</h4>
                        </div>
                        <div class="modal-body">
                          @foreach($orders as $order)
                            @if($purchase->order_number == $order->order_number)
                              <p>Product: {{$order->book_name}}</p>
                              <p>Quantity: {{$order->quantity}}</p>
                            @endif
                          @endforeach
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  <center> {{ $purchases->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('title', 'List of Users')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List of Users</div>
                <div class="panel-body">
                  @foreach($users as $user)
                    <div class="col-sm-6">
                      {{ $user->first_name }} {{ $user->last_name }}
                      @if ($user->role=='buyer')
                        &emsp;<button type="button" data-toggle="modal" data-target="#historyModal{{$user->id}}" >
                            <a>View Purchase History</a>
                        </button>
                      @endif
                       <br>{{ $user->email }}<br>
                      Role: {{$user->role}}<br><br><br>

                    </div>
                    <div id="historyModal{{$user->id}}" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">{{$user->first_name}} {{$user->last_name}}'s Purchase History</h4>
                          </div>
                          <div class="modal-body">
                            @php
                              $ordernum = 0;
                            @endphp
                              @foreach($purchases as $purchase)
                                @if($user->id == $purchase->customer_id)
                                  @if($ordernum != $purchase->order_number)
                                    <hr> Order Number: {{ $purchase->order_number}} <br><br>
                                    @php
                                      $ordernum = $purchase->order_number
                                    @endphp
                                  @endif
                                  Product: {{ $purchase->book_name}}<br>
                                  Quantity: {{$purchase->quantity}}<br>
                                @endif
                              @endforeach
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>

                    </div>
                  @endforeach
                </div>
                <center>{{ $users->links()}}</center>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('title', 'Shop')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if(Session::has('msg'))
              <div class = "alert alert-danger">{{ Session::get('msg') }}</div>
            @endif
            @if(Session::has('message'))
              <div class = "alert alert-success">{{ Session::get('message') }}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Shop <span style="padding-left: 800px;">
                  @if($auth->role == 'admin')
                    <a href="/books/create"><u>Add item</u></a></span>
                  @endif
                </div>
                <div class="panel-body">
                  @foreach($items as $item)
                    @if($item->isVisible)
                      <div class="col-sm-4">
                        {{ $item->book_name }} <br>
                        Available:
                        @if($item->status == 'available')
                          Yes
                        @else
                          No
                        @endif <br>
                        <button type="button" class="btn" data-toggle="modal" data-target="#shopModal{{$item->product_id}}">
                            <a>View Item</a>
                        </button>

                        @if ($auth->role == 'admin')
                        <button type="button" class = "btn"><a href="/books/{{ $item->product_id }}/edit">Edit Item</a></button>
                        <form class="" action="/books/{{ $item->product_id }}" method="post" onsubmit="return confirm('Are you sure?');">
                          <input type="hidden" name="_method" value="delete">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <input type="submit" name="name" value="Delete" class = "btn" style = "background-color:#FFFFFF">
                        </form>
                        @endif

                        <hr>
                      </div>

                      <div id="shopModal{{$item->product_id}}" class="modal fade" role = "dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">{{$item->book_name}}</h4>
                            </div>
                            <div class="modal-body">
                              <p>{{$item->description}}</p> <br>
                              <p>{{$item->available_items}} items left!</p>


                            </div>
                            <div class="modal-footer">
                              @if($auth->role == 'admin')
                                <button type="button" class="btn btn-default"><a href="/books/{{ $item->product_id }}/edit" style="color:#636b6f">Edit</a></button>
                              @else
                                  @if ($item->status=='available')
                                    <button type="button" class="btn btn-default"><a href="/add_to_cart/{{ $item->product_id }}" style="color:#636b6f">Add to Cart</a></button>
                                  @endif
                              @endif
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>

                      </div>
                    @endif
                  @endforeach
                </div>
                <center> {{ $items->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

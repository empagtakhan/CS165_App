@extends('layouts.app')
@section('title', 'Shop')
@section('content')

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
	<table id="cart" class="table table-hover table-condensed">
    @if(Session::has('message'))
      <div class = "alert alert-success">{{ Session::get('message') }}</div>
    @endif
    				<thead>
						<tr>
							<th style="width:50%">Product</th>
							<th style="width:10%">Quantity</th>
							<th style="width:8%"></th>
							<th style="width:22%" class="text-center"></th>
							<th style="width:10%"><a href="/remove_all" class="btn btn-danger" style="font-size:10px" onclick="return confirm('Are you sure?')">Remove all items</a></th>
						</tr>
					</thead>
					<tbody>
						<form class="form-horizontal" role = "form" method="post" action = '/submit'>
							  {{ csrf_field() }}
	            @foreach($cart_items as $item)
	  						<tr>
	  							<td data-th="Product">
	  								<div class="row">
	                    <div class="col-sm-10">
	                      <h5>{{$item->product_name}}</h5>
	                    </div>
	                    <div class="col-sm- hidden-xs"></div>
	  								</div>
	  							</td>
	                <td data-th="Price">
										<input name = "quantity[]" type="number" class="form-control text-center" value="1" min = 1 max = {{$item->max}}>
									</td>
	  							<td data-th="Quantity"></td>
	  							<td data-th="Subtotal" class="text-center"></td>
	  							<td class="actions" data-th="">
	                  	<a href='/remove/{{$item->product_id}}' style='color:#fff' onclick="return confirm('Are you sure?')" class = 'btn btn-danger'><i class="fa fa-trash-o" href='/remove/{{$item->product_id}}' onclick="return confirm('Are you sure?')"></i></a>

	  							</td>
	  						</tr>
	            @endforeach
						</tbody>
						<tfoot>
							<tr class="visible-xs">
							</tr>
							<tr>
								<td><a href="/shop" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
								<td colspan="2" class="hidden-xs"></td>
								<td class="hidden-xs text-center"></td>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<td><button type = "submit" class="btn btn-success btn-block" onclick="return confirm('Click OK to continue')">Checkout <i class="fa fa-angle-right"></i></button></td>
							</tr>
						</tfoot>
					</form>
				</table>
</div>

@endsection

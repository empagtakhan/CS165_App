@extends('layouts.app')
@section('title', 'Edit book')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Book</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/books/{{$book->product_id}}" id = 'addForm'>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('book_name') ? ' has-error' : '' }}">
                            <label for="book_name" class="col-md-4 control-label">Book Name</label>

                            <div class="col-md-6">
                                <input id="book_name" type="text" class="form-control" name="book_name" value="{{ $book->book_name }}" required >

                                @if ($errors->has('book_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('book_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">

                                <textarea name="description" rows="8" cols="40" form="addForm" class = 'form-control'>{{$book->description}}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('available_items') ? ' has-error' : '' }}">
                            <label for="available_items" class="col-md-4 control-label">Available Items</label>

                            <div class="col-md-6">
                                <input id="available_items" type="number" class="form-control" name="available_items" value="{{$book->available_items}}" min = 0 required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('available_items') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                  <input type="hidden" name="_method" value="put">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  Edit Book
                                </button>
                                &ensp; <a href="/shop"><u>Back</u></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

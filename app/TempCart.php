<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempCart extends Model
{
    public $primaryKey = 'product_id';
}

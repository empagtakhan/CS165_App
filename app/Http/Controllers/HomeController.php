<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = DB::table('orders')
                        ->join('orderdetails', 'orderdetails.order_number', 'orders.order_number')
                        ->join('books', 'books.product_id', 'orderdetails.product_id')
                        ->select('book_name', 'quantity', 'orders.order_number as order_number')
                        ->orderBy('orders.order_number', 'desc')
                        ->where('customer_id', Auth::user()->id)
                        ->paginate(10);
        return view('home', ['purchases' => $purchases]);
    }
}

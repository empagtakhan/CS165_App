<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
      $users = DB::table('users')->orderBy('role')->paginate(10);
      $purchases = DB::table('orders')
                      ->join('orderdetails', 'orderdetails.order_number', 'orders.order_number')
                      ->join('books', 'books.product_id', 'orderdetails.product_id')
                      ->select('book_name', 'quantity', 'orders.order_number as order_number', 'customer_id')
                      ->orderBy('orders.order_number', 'desc')
                      ->get();
      return view('user.show', ['users' => $users], ['purchases' => $purchases]);
    }

    public function edit($id)
    {
        $user = User::find($id);

        if (!$user)
        {
            abort(404);
        }

        return view('user.edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if($user->first_name == $request->first_name && $user->last_name == $request->last_name && $user->email == $request->email)
        {
            return redirect('home');
        }
        elseif ($user->email == $request->email)
        {
            $this->validate($request,[
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
            ]);
        }
        else
        {
            $this->validate($request,[
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
            ]);
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->save();

        return redirect('/home')->with('message', 'User profile has been updated!');
    }

    public function edit_password()
    {
        return view("user.change_password");
    }

    public function update_password(Request $request, $id)
    {
        $user = User::find($id);

        $this->validate($request,[
            'password' => 'required|min:6|confirmed',
        ]);

        if(Hash::check($request->old_password, $user->password))
        {
            if (Hash::check($request->password, $user->password))
            {
              return redirect('/change_password')->with('msg', 'New password must be different from the old password!');
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return redirect('/change_password')->with('message', 'Password has been changed!');
        }
        else
        {
            return redirect('/change_password')->with('msg', 'Password was incorrect! Please try again.');
        }



    }

}

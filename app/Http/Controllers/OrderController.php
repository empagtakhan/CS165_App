<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $purchases = DB::table('users')
                      ->join('orders', 'users.id', 'orders.customer_id')
                      ->select('first_name', 'last_name', 'orders.order_number as order_number')
                      ->orderBy('orders.order_number', 'desc')
                      ->paginate(20);
      $orders = DB::table('orders')
                    ->join('orderdetails', 'orders.order_number', 'orderdetails.order_number')
                    ->join('books', 'books.product_id', 'orderdetails.product_id')
                    ->select('book_name', 'orderdetails.order_number as order_number', 'quantity')
                    ->get();
      return view('order.index', ['purchases' => $purchases], ['orders' => $orders]);
    }
}

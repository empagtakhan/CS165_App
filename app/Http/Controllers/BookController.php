<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("book.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'book_name' => 'required|unique:books',
            'description' => 'required',
            'available_items' => 'required',
        ]);

        $book = new Book;
        $book->book_name = $request->book_name;
        $book->description = $request->description;
        $book->available_items = $request->available_items;
        $book->isVisible = 1;
        $book->save();

        return redirect('shop')->with('message', 'A new book has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        if(!$book){
            abort(404);
        }
        return view("book.edit")->with('book', $book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'book_name' => 'required',
            'description' => 'required',
            'available_items' => 'required',
        ]);

        $book = Book::where('product_id', $id)->first();
        $book->book_name = $request->book_name;
        $book->description = $request->description;
        $book->available_items = $request->available_items;
        $book->save();

        return redirect('shop')->with('message', $book->book_name. ' has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $book = Book::where('product_id', $id)->first();
        $book->isVisible = 0;
        $book->save();
        return redirect('shop')->with('message', 'Item has been deleted from shop!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Book;
use App\Order;
use App\OrderDetail;
use App\TempCart;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $items = DB::table('books')->orderBy('product_id')->paginate(15);
        return view('shop.index', ['items' => $items]);
    }

    public function cart()
    {
        $cart_items = DB::table('temp_carts')->get();
        return view('shop.cart')->with('cart_items', $cart_items);
    }

    public function add_to_cart($id)
    {
        $item = new TempCart;
        $item2 = TempCart::find($id);
        $book = Book::find($id);
        if (!$item2){
          $item->product_name = $book->book_name;
          $item->product_id = $book->product_id;
          $item->max = $book->available_items;

          $item->save();

          return redirect('shop')->with('message', "Item has been added to cart!");
        }
        else
        {
          return redirect('shop')->with('msg', 'Item is already in cart!');
        }
    }

    public function remove_from_cart($id)
    {
        DB::table('temp_carts')->where('product_id', $id)->delete();
        return redirect('cart')->with('message', "Item has been removed from cart!");
    }

    public function delete_all()
    {
        DB::table('temp_carts')->delete();
        return redirect('cart')->with('message', "All items from cart were deleted!");
    }

    public function submit(Request $request)
    {
        $items = DB::table('temp_carts')->get();
        $new_order = new Order;
        $new_order->order_number = (DB::table('orders')->max('order_number'))+1;
        $new_order->customer_id = Auth::user()->id;
        $new_order->save();
        $count = 0;
        foreach ($items as $item)
        {
            $new_orderdetail = new Orderdetail;
            $new_orderdetail->order_number = $new_order->order_number;
            $new_orderdetail->product_id = $item->product_id;
            $new_orderdetail->quantity = $request->input('quantity.'.(string)$count);
            $new_orderdetail->save();
            $count += 1;
            DB::table('books')->where('product_id', $new_orderdetail->product_id)->decrement('available_items', $new_orderdetail->quantity);
        }
        DB::table('temp_carts')->delete();
        return redirect('shop')->with('message', 'Your order has been processed. Thank you!');
    }
}
